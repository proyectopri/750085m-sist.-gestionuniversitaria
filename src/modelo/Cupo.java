/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class Cupo implements Serializable{

    private int cantidad;
    private String disponibles; //derivado
    private Programa programa;

    public Cupo(int cantidad, String disponibles, Programa programa) {
        this.cantidad = cantidad;
        this.disponibles = disponibles;
        this.programa = programa;
    }

    public int getCantidad() {
        return cantidad;
    }

    public String getDisponibles() {
        return disponibles;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setDisponibles(String disponibles) {
        this.disponibles = disponibles;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

}
