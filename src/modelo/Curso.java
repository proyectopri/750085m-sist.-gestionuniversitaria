/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author Usuario
 */
public class Curso implements Serializable{

    private int grupo;
    private int totalCupos; //derivado
    private Docente docente;
    private Asignatura asignatura;
    private LinkedList<Cupo> cupos;
    private Periodo periodo;
    private LinkedList<Horario> horarios;

    public Curso(int grupo, Docente docente, Asignatura asignatura, Periodo periodo) {
        this.grupo = grupo;
        this.totalCupos = 0;
        this.docente = docente;
        this.asignatura = asignatura;
        this.cupos = new LinkedList<>();
        this.periodo = periodo;
        this.horarios = new LinkedList<>();
    }

    public int getGrupo() {
        return grupo;
    }

    public int getTotalCupos() {
        return totalCupos;
    }

    public Docente getDocente() {
        return docente;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public LinkedList<Cupo> getCupos() {
        return cupos;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public LinkedList<Horario> getHorarios() {
        return horarios;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Curso other = (Curso) obj;
        if (this.grupo != other.grupo) {
            return false;
        }
        if (this.totalCupos != other.totalCupos) {
            return false;
        }
        if (!Objects.equals(this.docente, other.docente)) {
            return false;
        }
        if (!Objects.equals(this.asignatura, other.asignatura)) {
            return false;
        }
        if (!Objects.equals(this.cupos, other.cupos)) {
            return false;
        }
        if (!Objects.equals(this.periodo, other.periodo)) {
            return false;
        }
        if (!Objects.equals(this.horarios, other.horarios)) {
            return false;
        }
        return true;
    }
    

}
