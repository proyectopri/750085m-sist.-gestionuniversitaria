/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class Deuda implements Serializable{

    private String motivo;
    private Estado estado;

    public Deuda(String motivo, Estado estado) {
        this.motivo = motivo;
        this.estado = estado;
    }

    public String getMotivo() {
        return motivo;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
}
