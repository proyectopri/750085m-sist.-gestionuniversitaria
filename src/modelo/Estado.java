/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public enum Estado implements Serializable{
    ACTIVO, INACTIVO;
    
    public static Estado getACTIVO() {
        return ACTIVO;
    }

    public static Estado getINACTIVO() {
        return INACTIVO;
    }
}
