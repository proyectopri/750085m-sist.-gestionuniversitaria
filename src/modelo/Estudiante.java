/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Usuario
 */
public class Estudiante extends Persona  implements Serializable{

    private String codigo;
    private Programa programa;
    private LinkedList<Deuda> listDeudas;

    public Estudiante(String codigo, Programa programa, long identificacion, String nombres, String apellidos, Genero genero) {
        super(identificacion, nombres, apellidos, genero);
        this.codigo = codigo;
        this.programa = programa;
        this.listDeudas = new LinkedList<>();
    }

    public String getCodigo() {
        return codigo;
    }

    public Programa getPrograma() {
        return programa;
    }

    public LinkedList<Deuda> getListDeudas() {
        return listDeudas;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public void setListDeudas(LinkedList<Deuda> listDeudas) {
        this.listDeudas = listDeudas;
    }
    

}
