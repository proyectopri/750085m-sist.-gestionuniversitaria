/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public enum Genero implements Serializable{
    FEMENINO, MASCULINO;
    
    public static Genero getFEMENINO() {
        return FEMENINO;
    }

    public static Genero getMASCULINO() {
        return MASCULINO;
    }
}
