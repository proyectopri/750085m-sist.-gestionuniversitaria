/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalTime;

/**
 *
 * @author Usuario
 */
public class Horario implements Serializable{

    private DayOfWeek dia;
    private LocalTime horaInicio;
    private LocalTime horaFinalizacion;

    public Horario(DayOfWeek dia, LocalTime horaInicio, LocalTime horaFinalizacion) {
        this.dia = dia;
        this.horaInicio = horaInicio;
        this.horaFinalizacion = horaFinalizacion;
    }

    public DayOfWeek getDia() {
        return dia;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public LocalTime getHoraFinalizacion() {
        return horaFinalizacion;
    }
    

}
