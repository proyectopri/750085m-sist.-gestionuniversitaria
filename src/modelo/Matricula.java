/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author Usuario
 */
public class Matricula implements Serializable{

    private Curso curso;
    private Tabulado tabulado;
    private LocalDateTime matriculado;
    private LocalDateTime cancelado = null;

    public Matricula(Curso curso, Tabulado tabulado) {
        this.curso = curso;
        this.tabulado = tabulado;
        this.matriculado = LocalDateTime.now();
    }

    public Curso getCurso() {
        return curso;
    }

    public Tabulado getTabulado() {
        return tabulado;
    }

    public LocalDateTime getMatriculado() {
        return matriculado;
    }

    public LocalDateTime getCancelado() {
        return cancelado;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public void setTabulado(Tabulado tabulado) {
        this.tabulado = tabulado;
    }
    
}
