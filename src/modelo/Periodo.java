/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.time.Month;
import java.time.Year;
import java.util.Objects;

/**
 *
 * @author Usuario
 */
public class Periodo implements Serializable{

    private Month mesInicio;
    private Month mesFinal;

    private Year anioInicio;
    private Year anioFinal;

    public Periodo() {
        this.mesInicio = Month.FEBRUARY;
        mesInicio.ordinal();
        this.mesFinal = Month.AUGUST;
        mesFinal.ordinal();
        this.anioInicio = Year.now();
        this.anioFinal = Year.now();
    }

    public Month getMesInicio() {
        return mesInicio;
    }

    public Month getMesFinal() {
        return mesFinal;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Periodo other = (Periodo) obj;
        if (this.mesInicio != other.mesInicio) {
            return false;
        }
        if (this.mesFinal != other.mesFinal) {
            return false;
        }
        if (!Objects.equals(this.anioInicio, other.anioInicio)) {
            return false;
        }
        if (!Objects.equals(this.anioFinal, other.anioFinal)) {
            return false;
        }
        return true;
    }
    
    
}
