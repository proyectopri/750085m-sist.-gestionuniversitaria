/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author Usuario
 */
public class Tabulado implements Serializable{

    private Estudiante estudiante;
    private Periodo periodo;
    private LinkedList<Matricula> matriculas;

    public Tabulado(Estudiante estudiante, Periodo periodo) {
        this.estudiante = estudiante;
        this.periodo = periodo;
        this.matriculas = new LinkedList<>();
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public LinkedList<Matricula> getMatriculas() {
        return matriculas;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tabulado other = (Tabulado) obj;
        if (!Objects.equals(this.estudiante, other.estudiante)) {
            return false;
        }
        if (!Objects.equals(this.periodo, other.periodo)) {
            return false;
        }
        if (!Objects.equals(this.matriculas, other.matriculas)) {
            return false;
        }
        return true;
    }

}
