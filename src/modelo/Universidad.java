/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.LinkedList;

/**
 *
 * @author Usuario
 */
public class Universidad {

    private String nit;
    private String nombre;
    private String direccion;

    private LinkedList<Programa> programas;
    private LinkedList<Asignatura> asignaturas;
    private LinkedList<Curso> cursos;
    private LinkedList<Docente> docentes;
    private LinkedList<Estudiante> estudiantes;
    private LinkedList<Tabulado> tabulados;
    private LinkedList<Periodo> periodos;

    public Universidad(String nit, String nombre, String direccion) {
        this.nit = nit;
        this.nombre = nombre;
        this.direccion = direccion;

        this.asignaturas = new LinkedList<>();
        this.programas = new LinkedList<>();
        this.cursos = new LinkedList<>();
        this.docentes = new LinkedList<>();
        this.estudiantes = new LinkedList<>();
        this.tabulados = new LinkedList<>();
        this.periodos = new LinkedList<>();
    }

    public String getNit() {
        return nit;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    //Metodos add.
    public void addAsignatura(Asignatura asignatura) throws Exception {
        if (this.asignaturas.contains(asignatura)) {
            throw new Exception("El asignatura ya se eccuentra agregaddo");
        }
        this.asignaturas.add(asignatura);
    }

    public void addPrograma(Programa programa) throws Exception {
        if (this.programas.contains(programa)) {
            throw new Exception("El programa ya se encuentra agregado");
        }
        this.programas.add(programa);
    }

    public void addEstudiante(Estudiante estudiante) throws Exception {
        if (this.estudiantes.contains(estudiante)) {
            throw new Exception("El estudiante ya se eccuentra agregaddo");
        }
        this.estudiantes.add(estudiante);
    }

    public void addDocente(Docente docente) throws Exception {
        if (this.docentes.contains(docente)) {
            throw new Exception("El docente ya se eccuentra agregaddo");
        }
        this.docentes.add(docente);
    }

    public void addCurso(Curso curso) throws Exception {
        if (this.cursos.contains(curso)) {
            throw new Exception("El curso ya se eccuentra agregaddo");
        }
        this.cursos.add(curso);
    }

    public void addTabulado(Tabulado tabulado) throws Exception {
        if (this.tabulados.contains(tabulado)) {
            throw new Exception("El tabulado ya se eccuentra agregaddo");
        }
        this.tabulados.add(tabulado);
    }

    public void addPeriodo(Periodo periodo) throws Exception {
        if (this.periodos.contains(periodo)) {
            throw new Exception("El periodo ya se eccuentra agregaddo");
        }
        this.periodos.add(periodo);
    }

    //metodos buscar
    public Asignatura buscarAsignatura(String codigo) throws Exception {
        for (Asignatura objAsig : this.asignaturas) {
            if (objAsig.getCodigo().equals(codigo)) {
                return objAsig;
            }
        }
        throw new Exception("La asignatura no se encontro.");
    }

    public Docente buscarDocente(String profesion) throws Exception {
        for (Docente objDocente : this.docentes) {
            if (objDocente.getProfesion().equals(profesion)) {
                return objDocente;
            }
        }
        throw new Exception("El docente no se pudo encontrar.");
    }

    public Estudiante buscarEstudiante(String cod) throws Exception {
        for (Estudiante objEstudiante : this.estudiantes) {
            if (objEstudiante.getCodigo().equals(cod)) {
                return objEstudiante;
            }
        }
        throw new Exception("El estudiante no se pudo encontrar.");
    }

    public Programa buscarPrograma(String cod) throws Exception {
        for (Programa objPrograma : this.programas) {
            if (objPrograma.getCodigo().equals(cod)) {
                return objPrograma;
            }
        }
        throw new Exception("El programa no se pudo encontrar.");
    }

    public Curso buscarCurso(Asignatura asignatura) throws Exception {
        for (Curso objCurso : this.cursos) {
            if (objCurso.getAsignatura().equals(asignatura)) {
                return objCurso;
            }
        }
        throw new Exception("El curso no fue encontrado.");
    }

    //Mostrar listas.
    public LinkedList<Tabulado> obtenerTabulado(Estudiante estudiante) {
        LinkedList<Tabulado> listTabulados = new LinkedList<>();
        for (Tabulado objTabulado : this.tabulados) {
            if (objTabulado.getEstudiante().getCodigo() == estudiante.getCodigo()) {
                listTabulados.add(objTabulado);
            }
        }
        return listTabulados;
    }

    public LinkedList<Periodo> getPeriodos() {
        return periodos;
    }

}
