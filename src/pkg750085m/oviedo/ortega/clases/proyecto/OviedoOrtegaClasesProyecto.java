/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg750085m.oviedo.ortega.clases.proyecto;

import interfacesGUI.SRA;
import interfacesGUI.ventanaPrincipal;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Estudiante;
import modelo.Genero;
import modelo.Programa;
import modelo.Universidad;

/**
 *
 * @author Usuario
 */
public class OviedoOrtegaClasesProyecto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            Universidad universidad = new Universidad("12345", "Universidad del Valle", "cra30#34-56");
            
            Programa programa1 = new Programa("Tecnologia en sistemas", "2711");
            Estudiante estudiante1 = new Estudiante("1763840", programa1, 0, "Diana Marcela", "Oviedo", Genero.FEMENINO);

            universidad.addEstudiante(estudiante1);
            
            new ventanaPrincipal(universidad).setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(OviedoOrtegaClasesProyecto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
